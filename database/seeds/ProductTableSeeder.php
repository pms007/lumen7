<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 20) as $key => $value) {
        	Product::create([
        		'name' => $faker->sentence(5),
        		'price' => rand(1000,9999),
        		'description' => $faker->paragraph(10)
        	]);
        }
    }
}
